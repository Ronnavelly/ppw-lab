from django.test import TestCase, Client
import os
import base64


class Lab10UnitTest(TestCase):

    def test_lab_10_no_login(self):
        response = Client().get('/lab-10/login/')
        self.assertEqual(response.status_code, 200)

        response = Client().get('/lab-10/')
        self.assertEqual(response.status_code, 302)

        response = Client().get('/lab-10/dashboard/')
        self.assertEqual(response.status_code, 302)

        response = Client().get('/lab-10/movie/list/')
        self.assertEqual(response.status_code, 200)

        response = Client().get('/lab-10/movie/list/?judul=Titanic&tahun=1997')
        self.assertEqual(response.status_code, 200)

        MOVIE_ID = 'tt1375666'

        response = Client().get('/lab-10/movie/detail/' + MOVIE_ID + "/")
        self.assertEqual(response.status_code, 200)

        # hacking test
        response = Client().get('/lab-10/movie/detail/' + MOVIE_ID + "/")
        self.assertEqual(response.status_code, 200)

        MOVIE_ID_1 = 'tt0944947'

        client = Client()
        response = client.get(
            '/lab-10/movie/watch_later/add/' + MOVIE_ID + "/", follow=True)
        self.assertEqual(response.status_code, 200)

        response = client.get(
            '/lab-10/movie/watch_later/add/' + MOVIE_ID + "/", follow=True)
        self.assertEqual(response.status_code, 200)

        response = client.get(
            '/lab-10/movie/watch_later/add/' + MOVIE_ID_1 + "/", follow=True)
        self.assertEqual(response.status_code, 200)

        data = {
            'username': os.environ.get('SSO_USERNAME'),
            'password': os.environ.get('SSO_PASSWORD')
        }

        response = client.post(
            '/lab-10/custom_auth/login/', data=data, follow=True)
        self.assertEqual(response.status_code, 200)

        response = Client().get('/lab-10/movie/watch_later/')
        self.assertEqual(response.status_code, 200)

        TITLE, YEAR = "Titanic", "1997"

        response = Client().get(
            '/lab-10/api/movie/' + TITLE + "/" + YEAR + "/")
        self.assertEqual(response.status_code, 200)

        response = Client().get('/lab-10/api/movie/-/-/')
        self.assertEqual(response.status_code, 200)

        # pages > 1
        response = Client().get('/lab-10/api/movie/a/-/')
        self.assertEqual(response.status_code, 200)

    def test_lab_10_login(self):
        data = {
            'username': 'username',
            'password': 'wololo'
        }
        response = Client().post(
            '/lab-10/custom_auth/login/', data=data)
        self.assertEqual(response.status_code, 302)

        data = {
            'username': os.environ.get('SSO_USERNAME'),
            'password': os.environ.get('SSO_PASSWORD')
        }
        client = Client()
        response = client.post(
            '/lab-10/custom_auth/login/', data=data, follow=True)
        self.assertEqual(response.status_code, 200)

        MOVIE_ID = 'tt1375666'

        response = client.get('/lab-10/movie/detail/' +
                              MOVIE_ID + "/", follow=True)
        self.assertEqual(response.status_code, 200)

        response = client.get(
            '/lab-10/movie/watch_later/add/' + MOVIE_ID + "/", follow=True)
        self.assertEqual(response.status_code, 200)

        # hacking test
        response = client.get(
            '/lab-10/movie/watch_later/add/' + MOVIE_ID + "/", follow=True)
        self.assertEqual(response.status_code, 200)

        response = client.get('/lab-10/movie/watch_later/', follow=True)
        self.assertEqual(response.status_code, 200)

        response = client.get('/lab-10/custom_auth/logout/', follow=True)
        self.assertEqual(response.status_code, 200)
