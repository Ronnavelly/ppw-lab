from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.forms.models import model_to_dict
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers

from .models import Friend, Mahasiswa
from api_csui_helper.csui_helper import CSUIhelper
import os
import json

response = {}
csui_helper = CSUIhelper()

def index(request):
    # Page halaman menampilkan list mahasiswa yang ada
    # TODO berikan akses token dari backend dengan menggunakaan helper yang ada

    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()

    for mahasiswa in mahasiswa_list:
        nama_mhs = mahasiswa['nama']
        npm_mhs = mahasiswa['npm']
        Mahasiswa.objects.get_or_create(nama=nama_mhs, npm=npm_mhs)

    response['author'] = 'Zieggy Ronnavelly'
    friend_list = Friend.objects.all()
    list_mahasiswa = Mahasiswa.objects.all()
    paginator = Paginator(list_mahasiswa, 25) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        mahasiswa_page = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        mahasiswa_page = paginator.page(1)
    # except EmptyPage:
    #     # If page is out of range (e.g. 9999), deliver last page of results.
    #     mahasiswa_page = paginator.page(paginator.num_pages)
    response['friend_list'] = friend_list
    html = 'lab_7/lab_7.html'
    response['list_mahasiswa'] = mahasiswa_page
    return render(request, html, response)

def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

def friend_list_json(request):
    friends = Friend.objects.all()
    friend_list = []

    for friend in friends:
        friend_list.append(model_to_dict(friend))

    daftar = {'friend_list' : friend_list}
    return JsonResponse(daftar)

def isNpmValid(npm):
    friends = Friend.objects.all()

    for friend in friends:
        if friend.npm == npm:
            return False

    return True

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        valid = isNpmValid(npm)
        data = {"isValid" : valid}

        if(data["isValid"]):
            friend = Friend(friend_name=name, npm=npm)
            friend.save()
            data['friend'] = model_to_dict(friend)

        return JsonResponse(data)


@csrf_exempt
def delete_friend(request):
    if request.method == 'POST':
        friend_id = request.POST['id']
        Friend.objects.filter(id=friend_id).delete()
        data = {'id' : friend_id}
        return JsonResponse(data)

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
        'is_taken': not isNpmValid(npm) #lakukan pengecekan apakah Friend dgn npm tsb sudah ada
    }
    return JsonResponse(data)
