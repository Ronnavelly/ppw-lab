// FB initiation function
  window.fbAsyncInit = () => {
    FB.init({
      appId      : '128999441212336',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11'
    });
    $('body').append('<div id="fb-root"></div>');
    // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
    // dan jalankanlah fungsi render dibawah, dengan parameter true jika
    // status login terkoneksi (connected)

    // Hal ini dilakukan agar ketika web dibuka, dan ternyata sudah login, maka secara
    // otomatis akan ditampilkan view sudah login
    FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        console.log('connected');
        render(true);
      }
      else {
        console.log('not connected');
      }
  });
};

// Call init facebook. default dari facebook
(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}
(document, 'script', 'facebook-jssdk'));

// Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
// merender atau membuat tampilan html untuk yang sudah login atau belum
// Rubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
// Class-Class Bootstrap atau CSS yang anda implementasi sendiri
const render = loginFlag => {
    console.log('render');
    if (loginFlag) {
      console.log('Welcome!  Fetching your information.... ');
      FB.api('/me', function(response) {
        console.log('Good to see you, ' + response.name + '.');
      });    // Jika yang akan dirender adalah tampilan sudah login

    // Panggil Method getUserData yang anda implementasi dengan fungsi callback
    // yang menerima object user sebagai parameter.
    // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
        getUserData(user => {
        // Render tampilan profil, form input post, tombol post status, dan tombol logout
            $("#lab8").html(
              '<div class="profile">' +
                '<img class="cover" src="' + user.cover.source + ' alt="cover"/>' +
                '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
                '<div class="data">' +
                  '<h1>' + user.name + '</h1>' +
                  '<h2>' + user.about + '</h2>' +
                  '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
                '</div>' +
              '</div>' +
              '<input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" />' + '<br></br>' +
              '<button class="postStatus" onclick="postStatus()">Post ke Facebook</button>'
              //'<button class="logout" onclick="facebookLogout()">Logout</button>'
        );

        // Setelah merender tampilan diatas, dapatkan data home feed dari akun yang login
        // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
        // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
        // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
            getUserFeed(feed => {
                feed.data.map(value => {
                // Render feed, kustomisasi sesuai kebutuhan.
                    if (value.message && value.story) {
                        $('#lab8').append(
                            '<div class="feed">' +
                            '<p style="font-size:2rem">' + value.message + '</p>' +
                            '</div>' +
                            '<p style="font-weight:bold; color:#3b5998">' + value.story + '</p>'
                            +
                            '<button id="delete_'+ value.id +'" class="delete" onclick="deletePost(\'' + value.id + '\')">Delete</button>'

                        );
                    } else if (value.message) {
                        $('#lab8').append(
                            '<div class="feed">' +
                            '<p style="font-size:2rem">' + value.message + '</p>' +
                            '</div>' +
                            '<button id="delete_'+ value.id +'" class="delete" onclick="deletePost(\'' + value.id + '\')">Delete</button>'

                        );
                    } else if (value.story) {
                        $('#lab8').append(
                            '<p style="font-weight:bold; color:#3b5998">' + value.story + '</p>' +
                            '<button id="delete_'+ value.id +'" class="delete" onclick="deletePost(\'' + value.id + '\')">Delete</button>'

                        );
                }
              });
            });
        });
    } else {
        // Tampilan ketika belum login
        $('#lab8').html('<button class="login" onclick="facebookLogin()">Login</button>');
    }
};

const facebookLogin = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
    // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
    // pada scope yang ada.
    FB.login(function(response){
        console.log('Welcome!  Fetching your information.... ');
        FB.api('/me', function(response) {
          console.log('Good to see you, ' + response.name + '.');
     });
        location.reload();
    }, {scope:'public_profile,user_posts,publish_actions,email,user_about_me'})
};

const facebookLogout = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
    // ketika logout sukses.
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            FB.logout();
            render(false);
        }
    });
};


const getUserData = (render) => {
  FB.getLoginStatus((response) => {
    if (response.status === 'connected') {
      FB.api('/me?fields=id,name,cover,picture,about,email,gender', 'GET', (response) => {
        render(response);
      });
    }
  });
};


const getUserFeed = (fun) => {
// TODO: Implement Method Ini
// Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
// yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
// tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
// tersebut
    FB.getLoginStatus(function(response) {
        console.log('getUserFeed');
        if (response.status == 'connected') {
            FB.api('/me/feed', 'GET', function(response) {
                console.log(response);
                if (response && !response.error) {
                    /* handle the result */
                    console.log('ape');
                    fun(response);

                }
                else {
                    console.log('barbarik')
                }

            });
        }
    });
};

const postFeed = (message) => {
// Todo: Implement method ini,
// Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
// Melalui API Facebook dengan message yang diterima dari parameter.
    FB.api('/me/feed', 'POST', {message:message});
    render(false);
    render(true);
};

const postStatus = () => {
    const message = $('#postInput').val();
    postFeed(message);
};
const deletePost = (postID) => {
    FB.api(postID, 'delete', (response) => {
        if (!response || response.error) {
            alert('Tidak bisa mendelete, post tidak dibuat dalam app');
        }
        else {
            // $(".feed" + postID).remove();
            // $("#delete_" + postID).remove();
            render(false);
            render(true);
        }
    });
};
